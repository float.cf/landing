import angular from 'angular'

import sitesDirective from './sites/sites.directive'
import headerDirective from './header/header.directive'

export default angular.module('landing.common', [])
  .directive('landingSites', sitesDirective)
  .directive('landingHeader', headerDirective)
  .name
