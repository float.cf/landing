export default () => {
  return {
    template: require('./header.html'),
    controller: ($scope, $state) => {
      $scope.state = $state.current.name
    }
  }
}
