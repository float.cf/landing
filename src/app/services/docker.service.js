class Docker {
  constructor () {
    this.containers = []
    this.updated = new Date().toString()
  }

  $onInit () {

  }

  getContainers () {
    return this.containers
  }

  setContainers (containers) {
    this.containers = containers
    this.updated = new Date().toString()
  }
}

Docker.$inject = []

export default Docker
