import angular from 'angular'
import uiRouter from '@uirouter/angularjs'

import LandingComponent from './landing/landing.component'
import LandingAbout from './about/about.component'
import LandingMe from './me/me.component'

export default angular.module('landing.components', [uiRouter])
  .component('landingLanding', LandingComponent)
  .component('landingAbout', LandingAbout)
  .component('landingMe', LandingMe)
  .config(($stateProvider, $urlRouterProvider) => {
    // 'ngInject'
    // console.log('Loading routes...')
    $stateProvider
      .state('landing', {
        url: '/',
        component: 'landingLanding'
      })
      .state('about', {
        url: '/about',
        component: 'landingAbout'
      })
      .state('me', {
        url: '/me',
        component: 'landingMe'
      })
    $urlRouterProvider.otherwise('/')
  })
  .name
