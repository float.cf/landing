class LandingController {
  constructor ($rootScope, $sync, $docker, $nginx, $state) {
    this.$rootScope = $rootScope
    this.$docker = $docker
    this.$nginx = $nginx
    this.$state = $state
  }
}

LandingController.$inject = ['$rootScope', '$sync', '$docker', '$nginx', '$state']

export default LandingController
