FROM mhart/alpine-node:9

RUN mkdir -p /code
WORKDIR code
ADD . /code

RUN npm install
RUN npm run build
RUN npm install -g http-server

CMD http-server /code/public
EXPOSE 8080
